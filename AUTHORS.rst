=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* revesansparole, <revesansparole@gmail.com>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* revesansparole <revesansparole@gmail.com>
* Jerome Chopard <revesansparole@gmail.com>

.. #}
